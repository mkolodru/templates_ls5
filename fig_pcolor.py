import numpy
import pylab
import mkplot

x=numpy.arange(0,10)
y=numpy.arange(-11,0)
z=numpy.zeros((len(x),len(y)))

for j in xrange(len(x)):
    for k in xrange(len(y)):
        z[j,k]=x[j]+y[k]**2

(y_g,x_g)=mkplot.mk_meshgrid(y,x)
print 'x_g.shape='+str(x_g.shape)
pylab.pcolor(x_g,y_g,z)
pylab.xlim(min(x)-0.5,max(x)+0.5)
pylab.ylim(min(y)-0.5,max(y)+0.5)
pylab.colorbar()
pylab.show()
