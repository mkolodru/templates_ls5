import numpy
import glob
import sys
from extrap_to_zero import *

def matches(vmap,col_lst,row1,row2):
	for col in col_lst:
		if vmap[row1,col]!=vmap[row2,col]:
			return False
	return True

if len(sys.argv) < 2:
	print 'Proper usage: extrap_obs.py $obs [$col_names]'
	sys.exit()

obs_str=sys.argv[1]

vmap=numpy.loadtxt('versionmap.dat',skiprows=1)
labels=open('versionmap.dat').readline().split()
vnum_col=labels.index('vnum')
dt_col=labels.index('dt')

col_lst=[]

for col_name in sys.argv[2:]:
	col_lst.append(labels.index(col_name))

used=numpy.zeros((vmap.shape[0]),dtype=int)

for row_init in xrange(vmap.shape[0]):
	if used[row_init]==1:
		continue

	vnum_init=int(vmap[row_init,vnum_col])

	sys.stderr.write('Version '+str(vnum_init)+' matches to ')

	dt_lst=[]
	obs_lst=[]
	first=True
	for row in xrange(row_init,vmap.shape[0]):
		if used[row]==0 and matches(vmap,col_lst,row_init,row):
			used[row]=1
			vnum=int(vmap[row,vnum_col])
			if first:
				first=False
			else:
				sys.stderr.write(', ')
			sys.stderr.write(str(vnum))
			dt_lst.append(vmap[row,dt_col])
			obs_lst.append(numpy.loadtxt('v'+str(vnum)+'/build/'+obs_str+'.out'))

	sys.stderr.write('\n')
	
	(val,sval)=extrap_to_zero(obs_lst,dt_lst)
	numpy.savetxt('v'+str(vnum_init)+'/build/'+obs_str+'_extrap.out',val)
	numpy.savetxt('v'+str(vnum_init)+'/build/s'+obs_str+'_extrap.out',sval)
